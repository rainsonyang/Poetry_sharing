package cn.fanrunqi.materiallogin.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import java.util.List;

import cn.fanrunqi.materiallogin.R;
import vo.Message;


public class GridViewAdapter extends BaseSwipeAdapter {

    private Context mContext;
    private List<Message> messageList;

    public GridViewAdapter(Context mContext,List<Message> messageList) {
        this.mContext = mContext;
        this.messageList = messageList;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(mContext).inflate(R.layout.grid_item, null);
    }

    @Override
    public void fillValues(int position, View convertView) {
        //TextView t = (TextView)convertView.findViewById(R.id.trash);

        TextView title = (TextView)convertView.findViewById(R.id.title);
        TextView content = (TextView)convertView.findViewById(R.id.content);
        TextView time = (TextView)convertView.findViewById(R.id.time);

        Message message=messageList.get(position);

        Log.e("message",message.toString());
        title.setText(message.getTitle());
        content.setText(message.getContent());
        time.setText(message.getTime());


    }

    @Override
    public int getCount() {

        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
