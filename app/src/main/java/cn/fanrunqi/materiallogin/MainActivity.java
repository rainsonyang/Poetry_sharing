package cn.fanrunqi.materiallogin;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.transition.Explode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import vo.Message;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.et_username)
    EditText etUsername;
    @InjectView(R.id.et_password)
    EditText etPassword;
    @InjectView(R.id.bt_go)
    Button btGo;
    @InjectView(R.id.cv)
    CardView cv;
    @InjectView(R.id.fab)
    FloatingActionButton fab;
    List<Message> messageList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

    }

    @OnClick({R.id.bt_go, R.id.fab})
    public void onClick(View view){
        switch (view.getId()) {
            case R.id.fab:
                getWindow().setExitTransition(null);
                getWindow().setEnterTransition(null);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options =
                            ActivityOptions.makeSceneTransitionAnimation(this, fab, fab.getTransitionName());
                    startActivity(new Intent(this, RegisterActivity.class), options.toBundle());
                } else {
                    startActivity(new Intent(this, RegisterActivity.class));
                }
                break;
            case R.id.bt_go:
                try{
                    Explode explode = new Explode();
                    explode.setDuration(500);
                    getWindow().setExitTransition(explode);
                    getWindow().setEnterTransition(explode);
                    final ActivityOptionsCompat oc2 = ActivityOptionsCompat.makeSceneTransitionAnimation(this);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(login()){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this,"SUCCESS",Toast.LENGTH_SHORT).show();

                                        Intent i2 = new Intent(getApplicationContext(),LoginSuccessActivity.class);
                                        //i2.putExtra("messageList", (Serializable) messageList);
                                        startActivity(i2, oc2.toBundle());
                                    }
                                });
                            }else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this,"用户名或密码错误，请重新登录",Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }
                    }).start();

                }catch (Exception e){
                    e.getStackTrace();
                }

//
                break;
        }
    }

    public boolean login() {
        boolean flag=false;
        String url="http://192.168.43.115:8080/user/login?";
        try {
            Log.e("etUsername.getText()",etUsername.getText().toString());
            Log.e("etPassword.getText()()",etPassword.getText().toString());
            if(etUsername.getText()!=null||etPassword.getText()!=null){
                url=url+"name="+etUsername.getText()+"&"+"password="+etPassword.getText();
                OkHttpClient client=new OkHttpClient();
                Request request=new Request.Builder().url(url).build();
                Response response=client.newCall(request).execute();//接收
                String responsedata=response.body().string();
                JSONObject jsonObject=new JSONObject(responsedata);
                String code=jsonObject.getString("code");
                int userid=jsonObject.getInt("userid");
                SharedPreferences.Editor editor=getSharedPreferences("data",MODE_PRIVATE).edit();
                editor.putInt("userid",userid);
                editor.apply();
                /*JSONArray messages=jsonObject.getJSONArray("messageList");
                Log.e("messages+++",messages.toString());
                messageList = new ArrayList<Message>();
                for (int i = 0;i < messages.length();i++){
                    JSONObject object=messages.getJSONObject(i);
                    Message message=new Message();
                    message.setMessageid((Integer) object.get("messageid"));
                    message.setTitle(object.getString("title"));
                    message.setContent(object.getString("content"));
                    message.setTime(object.getString("time"));
                    Log.e("mainactivityLogin",message.toString());
                    messageList.add(message);
                }*/
                Log.e("code",code);
                Log.e("responsedata",responsedata);
                if(code.equals("1")){
                    flag=true;
                }
            }
        }catch (IOException io){
            io.getStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  flag;
    }
}
