package cn.fanrunqi.materiallogin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WriteMessageActivity extends AppCompatActivity {
    private Button button;
    private EditText newTitle;
    private EditText newContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_message);


        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(addMessage()){
                            Intent intent=new Intent(WriteMessageActivity.this,LoginSuccessActivity.class);
                            startActivity(intent);
                        }
                    }
                }).start();

            }
        });
    }

    public boolean addMessage(){
        boolean flag=false;
        String url="http://192.168.43.115:8080/message/addMessage.action?";
        try {
            newTitle=(EditText)findViewById(R.id.newtitle);
            newContent=(EditText)findViewById(R.id.newcontent);
            String t= String.valueOf(newTitle.getText());
            String c= String.valueOf(newContent.getText());
            if(t!=null||c!=null){
                SharedPreferences prep=getSharedPreferences("data",MODE_PRIVATE);
                int userid=prep.getInt("userid",1);
                url=url+"title="+t+"&"+"content="+c+"&"+"userid="+userid;
                OkHttpClient client=new OkHttpClient();
                Request request=new Request.Builder().url(url).build();
                Response response=client.newCall(request).execute();//接收
                String responsedata=response.body().string();
                JSONObject jsonObject=new JSONObject(responsedata);
                String code=jsonObject.getString("code");
                Log.e("code",code);
                Log.e("responsedata",responsedata);
                if(code.equals("1")){
                    flag=true;
                }
            }
        }catch (IOException io){
            io.getStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  flag;

    }
}
