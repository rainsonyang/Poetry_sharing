package cn.fanrunqi.materiallogin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.util.Attributes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import cn.fanrunqi.materiallogin.adapter.GridViewAdapter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import vo.Message;

public class LoginSuccessActivity extends AppCompatActivity {
    private FloatingActionButton floatingActionButton;
    private List<Message> messageList;
    private GridView gridView;
    private ImageView imageView;
    private LinearLayout linearLayout;
    private SwipeLayout swipeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_success);
        gridView = (GridView)findViewById(R.id.gridview);
        Explode explode = new Explode();
        explode.setDuration(500);
        getWindow().setExitTransition(explode);
        getWindow().setEnterTransition(explode);



        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    SharedPreferences prep=getSharedPreferences("data",MODE_PRIVATE);
                    int userid=prep.getInt("userid",1);
                    String url="http://192.168.43.115:8080/message/listMessage.action?userid="+userid;
                    OkHttpClient client=new OkHttpClient();
                    Request request=new Request.Builder().url(url).build();
                    Response response=client.newCall(request).execute();//接收
                    String responsedata=response.body().string();
                    JSONObject jsonObject=new JSONObject(responsedata);
                    String code=jsonObject.getString("code");

                    JSONArray messages=jsonObject.getJSONArray("messageList");
                    messageList = new ArrayList<Message>();

                    for (int i = 0;i < messages.length();i++){
                        JSONObject object=messages.getJSONObject(i);
                        Message message=new Message();

                        message.setMessageid((Integer) object.get("messageid"));
                        message.setTitle(object.getString("title"));
                        message.setContent(object.getString("content"));
                        message.setTime(object.getString("time"));

                        messageList.add(message);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            GridViewAdapter adapter = new GridViewAdapter(getApplicationContext(),messageList);
                            adapter.setMode(Attributes.Mode.Multiple);
                            gridView.setAdapter(adapter);
                            gridView.setSelected(false);
                        }
                    });
                }catch (IOException io){
                    io.getStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        floatingActionButton=(FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginSuccessActivity.this,WriteMessageActivity.class);
                startActivity(intent);

            }
        });

//        swipeLayout=(SwipeLayout)findViewById(R.id.swipe) ;
//        linearLayout= (LinearLayout)swipeLayout.findViewById(R.id.line1);

        /*imageView= (ImageView) gridView.findViewById(R.id.trash);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("aa","ss");
            }
        });*/


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(LoginSuccessActivity.this,updateActivity.class);
                startActivity(intent);
                Log.e("onItemClick","onItemClick:" + position);
            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, int position, long id) {

                final int messageid=messageList.get(position).getMessageid();

                boolean flag=false;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String url="http://192.168.43.115:8080/message/delMessage.action?messageid="+messageid;
                            OkHttpClient client=new OkHttpClient();
                            Request request=new Request.Builder().url(url).build();
                            Response response=client.newCall(request).execute();
                            String responsedata=response.body().string();
                            JSONObject jsonObject=new JSONObject(responsedata);
                            String code=jsonObject.getString("code");
                            if(code.equals("1")){
                                SharedPreferences prep=getSharedPreferences("data",MODE_PRIVATE);
                                int userid=prep.getInt("userid",1);
                                String url2="http://192.168.43.115:8080/message/listMessage.action?userid="+userid;
                                OkHttpClient client2=new OkHttpClient();
                                Request request2=new Request.Builder().url(url2).build();
                                Response response2=client.newCall(request2).execute();//接收
                                String responsedata2=response2.body().string();
                                JSONObject jsonObject2=new JSONObject(responsedata2);
                                String code2=jsonObject.getString("code");

                                JSONArray messages=jsonObject2.getJSONArray("messageList");
                                messageList = new ArrayList<Message>();
                                for (int i = 0;i < messages.length();i++){
                                    JSONObject object=messages.getJSONObject(i);
                                    Message message=new Message();

                                    message.setMessageid((Integer) object.get("messageid"));
                                    message.setTitle(object.getString("title"));
                                    message.setContent(object.getString("content"));
                                    message.setTime(object.getString("time"));

                                    messageList.add(message);
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        GridViewAdapter adapter = new GridViewAdapter(getApplicationContext(),messageList);
                                        adapter.setMode(Attributes.Mode.Multiple);
                                        gridView.setAdapter(adapter);
                                        gridView.setSelected(false);
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();




                return flag;
            }
        });


        gridView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("onItemSelected","onItemSelected:" + position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


}
